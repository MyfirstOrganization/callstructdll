// CStructDll.cpp : 定义 DLL 应用程序的导出函数。
//

#include "stdafx.h"
#include "CStructDll.h"
//调用接口含普通变量的指针
extern "C" _declspec(dllexport) int fuAdd(int* num1,int* num2,int* outnum)
{
	 *outnum=*num1 + *num2;
	 return *outnum;
}
//调用接口含结构体
extern "C" _declspec(dllexport) int fuAddStruct(mybuf ms )
{
	
	return ms.num1+ms.num2;
}
//调用接口含结构体指针
extern "C" _declspec(dllexport) mybuf*  funStructPt(int *num)
{
	int number = 10;
	mybuf *mspt = new mybuf[number];
	
	for (int i=0;i<10;i++)
	{
		mspt[i].num1 = i;
		mspt[i].num2 = i;
	}
	*num = number;
	
	return mspt;
}
//调用接口含结构体中含有结构体指针
extern "C" _declspec(dllexport)  int  funStructPtReturn(Mesh* mesh)
{
	int number = 10;
	//Mesh *stlmesh=new Mesh[number];
	Mesh stlmesh ;
	stlmesh.triangles = new Triangle[number];
	
	//stlmesh.normals = new Point[number];
	stlmesh.count = number;
	//stlmesh->normals = new Point[number];
	for (int i=0;i<number;i++)
	{
	
		for (int j=0;j<3;j++)
		{
			stlmesh.triangles[i].vertexs[j].x = i;
			stlmesh.triangles[i].vertexs[j].y = i;
			stlmesh.triangles[i].vertexs[j].z = i;
		}
		stlmesh.triangles[i].normal.x = i;
		stlmesh.triangles[i].normal.y = i;
		stlmesh.triangles[i].normal.z = i;
		/*stlmesh.normals[i].x = i;
		stlmesh.normals[i].y = i;
		stlmesh.normals[i].z = i;*/
		
	}
	*mesh = stlmesh;

	//for (int j = 0; j<number; j++)
	//{

	// mesh->points[j].x = stlmesh->points[j].x;
	// mesh->points[j].y = stlmesh->points[j].y;
	// mesh->points[j].z = stlmesh->points[j].z;
	// mesh->normals[j].x = stlmesh->normals[j].x;
	// mesh->normals[j].y = stlmesh->normals[j].y;
	// mesh->normals[j].z = stlmesh->normals[j].z;
	//}
	
	return stlmesh.count;
}

