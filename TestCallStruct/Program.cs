﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;
using System.IO;
namespace TestCallStruct
{
    class Program
    {
        //dll声明处
        [DllImport(@"CStructDll.dll", EntryPoint = "fuAdd")]
        public static extern int fuAdd( ref int num1,ref int num2,ref int c);
        [StructLayout(LayoutKind.Sequential)]
        public struct MyBuf
        {
            public int num1;
            public int num2;
            public MyBuf(int n1, int n2)
            {
                num1 = n1;
                num2 = n2;
            }
        }
        [DllImport(@"CStructDll.dll", EntryPoint = "fuAddStruct")]
        public static extern int fuAddStruct(MyBuf ms);
        [DllImport(@"CStructDll.dll", EntryPoint = "fuAddStructPt")]
        public static extern IntPtr fuAddStructPt(ref int num);
        [StructLayout(LayoutKind.Sequential)]
        public struct Point
        {
            float x;
            float y;
            float z;
            public Point(float a,float b,float c)
            {
                this.x = a;
                this.y = b;
                this.z = c;
            }
        }
        [StructLayout(LayoutKind.Sequential)]
        public struct Triangle
        {
            Point normal;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 3)]
            Point[] vertexs;
        }
        [StructLayout(LayoutKind.Sequential)]
        public struct Mesh
        {
            public IntPtr triangles;
           // public IntPtr normals;
            public int count;//返回mesh的三角面片数
        }
        [DllImport(@"CStructDll.dll", EntryPoint = "funStructPtReturn")]
        public static extern int funStructPtReturn(ref Mesh mesh);

        static void Main(string[] args)
        {
            int a=3, b=2,c=0;
            int num = 0;
            MyBuf ms = new MyBuf();
            ms.num1 = 3;
            ms.num2 = 2;
            List<MyBuf> mspt = new List<MyBuf>();
           // int size2 = Marshal.SizeOf(typeof(MyBuf)) * 10;
           // IntPtr pt = Marshal.AllocHGlobal(size2);
            int sum1 = fuAdd(ref a, ref b,ref c);
            int sum2 = fuAddStruct(ms);
            // IntPtr meshpt = Marshal.AllocHGlobal(Marshal.SizeOf(typeof(Mesh)));
            // IntPtr pt = fuAddStructPt(ref num);
            //IntPtr meshpt = funStructPtReturn(ref count);
            // Mesh mesh = (Mesh)Marshal.PtrToStructure(meshpt,typeof(Mesh));
            // IntPtr pointsAddress=mesh.points_address;//获取点数据的地址
            //  IntPtr normalsAddress=mesh.normals_address;
            // Point[] points = new Point[count];
            // Point[] normals = new Point[count];
            //使用地址来获取需要的内存块中的数据
            //Marshal.Copy(pointsAddress,points,0,count);
            Triangle triangles = new Triangle();
            // Point points=new Point();
           //  Point normals=new Point();
            IntPtr triangles_buf = Marshal.AllocCoTaskMem(Marshal.SizeOf(triangles));
           //  IntPtr normals_buf = Marshal.AllocCoTaskMem(Marshal.SizeOf(normals));
            //IntPtr points_buf = Marshal.AllocCoTaskMem(Marshal.SizeOf(points));
            Mesh mesh =new Mesh();
            mesh.triangles = triangles_buf;
           // mesh.normals = normals_buf;
            //  mesh.points = points_buf;
            mesh.count = 0;
            int res = funStructPtReturn(ref mesh);
            // List<Point> Mpoints = new List<Point>();
           // List<Point> sumNormals = new List<Point>();
            List<Triangle> sumTriangles = new List<Triangle>();
            for (int i=0;i<res;i++)
            {
                IntPtr pp = new IntPtr(mesh.triangles.ToInt64() + Marshal.SizeOf(typeof(Triangle)) * i);
                sumTriangles.Add((Triangle)Marshal.PtrToStructure(pp, typeof(Triangle)));
              //  IntPtr pn = new IntPtr(mesh.normals.ToInt64() + Marshal.SizeOf(typeof(Point)) * i);
              //  sumNormals.Add((Point)Marshal.PtrToStructure(pn, typeof(Point)));
            }
            
           
            Console.WriteLine("num:{0}",num);
            Console.WriteLine("count:{0}", mesh.count);
            //for (int i=0;i<num;i++)
            //{
            //    IntPtr ptr = new IntPtr(pt.ToInt64() + Marshal.SizeOf(typeof(MyBuf)) * i);
            //    mspt.Add((MyBuf)Marshal.PtrToStructure(ptr, typeof(MyBuf)));

            //}
            //for (int j=0;j<mspt.Count;j++)
            //{
            //    Console.WriteLine("{0}:{1},{2}",j+1,mspt[j].num1, mspt[j].num2);
            //}
            
           // Console.WriteLine("fuAdd result:{0}+{1}={2}", a,b,c);
           // Console.WriteLine("fuAddStruct result:{0}+{1}={2}", ms.num1,ms.num2,sum2);
           // Console.WriteLine("fuAddStructpt result:{0}+{1}={2}", ms.num1, ms.num2, num);
            Console.ReadKey();
        }
    }
}
